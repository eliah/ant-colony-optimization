import time
from benchmark import run_benchmark
from analysis import Analysis
from helpers import print_finished
from file_setup import setup

if __name__ == '__main__':
    start = time.time()
    setup()
    run_benchmark()
    analysis = Analysis()
    analysis.run_analysis()
    print_finished(start)