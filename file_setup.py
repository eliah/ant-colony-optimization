from datetime import datetime
import os

base_folder = "results"
now = datetime.now()
formatted_date = now.strftime("%d-%m")
formatted_time = now.strftime("%H-%M-%S")
run_identifier = formatted_time
csv_folder = os.path.join(base_folder, "csv", formatted_date)
CSV_PATH = os.path.join(csv_folder, run_identifier + ".csv")
log_folder = os.path.join(base_folder, "log", formatted_date)
LOG_PATH = os.path.join(log_folder, run_identifier + ".txt")
FIGURES_FOLDER = os.path.join(base_folder, "figures", formatted_date, run_identifier)

def setup():
    os.makedirs(csv_folder, exist_ok=True)
    os.makedirs(log_folder, exist_ok=True)