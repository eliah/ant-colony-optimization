import time
import os
from datetime import datetime

from analysis import Analysis
from helpers import print_finished, printt
from file_setup import base_folder


CSV_DATE = "11-07"
CSV_NAME = "10-53-14"

# target, column pairs to drop from data
TO_DROP = [
    # ("Threshold Accepting", "Algorithm"),
    # ("Simulated Annealing", "Algorithm"),



    # # ("Greedy Nx", "Algorithm"),
    # ("Christofides", "Algorithm")
]

ADD_LABELS = False

NAME_DICT = {
    "Greedy Nx": "Greedy",
    "Simulated Annealing": "Simulated Annealing",
    "Threshold Accepting": "Threshold Accepting",
    "NN": "Nearest Neighbor",
    "Two Opt 100": "2-Opt",
    "ACO adaptive v2 2": "Ant Colony Optimization"
}

NAME_DICT_SHORT = {
    "Greedy Nx": "G",
    "Simulated Annealing": "SA",
    "Threshold Accepting": "TA",
    "NN": "NN",
    "Two Opt 100": "2O",
    "ACO adaptive v2 2": "ACO"
}

def get_paths():
    now = datetime.now()
    analysis_identfier = now.strftime("%H-%M-%S")

    csv_path = os.path.join(base_folder, "csv", CSV_DATE, CSV_NAME + ".csv")
    fig_folder = os.path.join(base_folder, "figures", CSV_DATE, CSV_NAME, analysis_identfier)
    return csv_path, fig_folder

if __name__ == '__main__':
    start = time.time()
    csv_path, fig_folder = get_paths()
    printt(f"Running analysis on {csv_path}")
    analysis = Analysis(csv_path, fig_folder, NAME_DICT, NAME_DICT_SHORT, ADD_LABELS)
    for target, col in TO_DROP:
        analysis.drop_rows(target, col)
    analysis.run_analysis()
    print_finished(start)