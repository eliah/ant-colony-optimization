import pandas as pd
from file_setup import CSV_PATH, FIGURES_FOLDER
import matplotlib.pyplot as plt
import os
from helpers import printt

TIME_AXIS_LABEL = "Time (s)"
COST_AXIS_LABEL = "Normalized cost"
SIZE_AXIS_LABEL = "Graph size (vertices)"
LINE_WIDTH_PTS = 290    # retrieved with \the\linewidth
TEXT_HEIGHT_PTS = 215   # retrieved with \the\textheight
FIG_WIDTH = LINE_WIDTH_PTS / 36
FIG_HEIGHT = TEXT_HEIGHT_PTS / 36


class Analysis():
    def __init__(self, csv_path=CSV_PATH, figures_folder=FIGURES_FOLDER, name_dict={}, name_dict_short = {}, add_labels = True):
        self.csv_path = csv_path
        self.figures_folder = figures_folder
        self.name_dict = name_dict
        self.name_dict_short = name_dict_short
        self.add_labels = add_labels
        self.data = self.get_normalized_csv()
        os.makedirs(self.figures_folder, exist_ok=True)

    def get_normalized_csv(self):
        data = pd.read_csv(self.csv_path)
        # find min cost
        mins = data.drop(columns=["Algorithm", "Time"]).groupby(["Size", "Run"]).min().reset_index()
        mins.rename(columns={"Cost": "Min Cost"}, inplace=True)
        # add column with normalized cost
        normalized = data.merge(mins)
        normalized["Normalized Cost"] = normalized["Cost"] / normalized["Min Cost"]
        # average over runs and drop useless columns
        return normalized.groupby(["Algorithm", "Size"]).mean().reset_index().drop(columns=["Run", "Cost", "Min Cost"])

    # drops rows containing target in column col from data
    def drop_rows(self, target, col):
        self.data = self.data[self.data[col] != target]

    def run_analysis(self):
        self.plot_time()
        self.plot_time(logscale=True)
        if self.data["Algorithm"].nunique() > 1:
            self.plot_cost()
            self.plot_cost(logscale=True)
            # self.plot_sizes()
            # self.plot_sizes(logscale=True)

    def plot_time(self, logscale=False):
        self.plot(y_axis="Time",
                  y_label=TIME_AXIS_LABEL,
                  title="Time taken per algorithm",
                  filename="time",
                  logscale=logscale,
                  drop_optimum=True)

    def plot_cost(self, logscale=False):
        self.plot(y_axis="Normalized Cost",
                  y_label=COST_AXIS_LABEL,
                  title="Normalized cost per algorithm",
                  filename="cost",
                  logscale=logscale,
                  drop_optimum=True)

    def plot(self, y_axis, y_label, title, filename, x_axis="Size", x_label=SIZE_AXIS_LABEL, logscale=False, drop_optimum=False):
        fig, ax = plt.subplots()
        grouped = self.data.groupby("Algorithm")
        for algorithm, group in grouped:
            if drop_optimum and algorithm == "Optimum":
                continue
            x_ax = group[x_axis]
            y_ax = group[y_axis]
            ax.plot(x_ax, y_ax, label=self.name_dict.get(algorithm, algorithm), marker='o', markersize=5)
            if self.add_labels:
                ax.annotate(self.name_dict_short.get(algorithm, algorithm), (x_ax.iloc[len(x_ax) - 1], y_ax.iloc[len(y_ax) - 1]), textcoords="offset points", xytext=(0, 5), ha='center')

        if logscale:
            ax.set_yscale('log')

        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        ax.legend()
        ax.set_title(title + (" (log scale)" if logscale else ""))
        ax.grid(True)

        plt.xticks(rotation=90)
        plt.tight_layout()

        file_suffix = "_logscale" if logscale else ""
        file_path = os.path.join(self.figures_folder, filename + file_suffix + ".pdf")
        fig.savefig(file_path, format="pdf")
        plt.close(fig)
        printt(f"Saved {file_path}")

    def plot_sizes(self, logscale=False):
        grouped = self.data.groupby("Size")
        for size, group in grouped:
            fig, ax = plt.subplots()
            for _, row in group.iterrows():
                x = row["Time"]
                y = row["Normalized Cost"]
                label = row["Algorithm"]
                ax.scatter(x, y, label=label)
                ax.annotate(label, (x, y), textcoords="offset points", xytext=(0, 5), ha='center', fontsize=10, rotation=0)

            if logscale:
                ax.set_yscale('log')

            ax.set_xlabel(TIME_AXIS_LABEL)
            ax.set_ylabel(COST_AXIS_LABEL)
            ax.legend()
            ax.set_title(f"Time vs cost for {size} vertices" + (" (log scale)" if logscale else ""))
            ax.grid()

            file_suffix = "_logscale" if logscale else ""
            file_path = os.path.join(self.figures_folder, f"size_{size}" + file_suffix + ".pdf")
            fig.savefig(file_path, format="pdf")
            plt.close(fig)
            printt(f"Saved {file_path}")
