import random
import networkx as nx
import numpy as np
import tsplib95
import os
import pandas as pd


# Custom graph implementation that includes pheromone levels for every edge
# Nodes is number of nodes
# Edges is a 2d symmetric matrix, entry [i,j] is the cost of edge between node i to node j (or j to i)
class Graph():
    def __init__(self, nodes, edges):
        self.nodes = nodes
        self.edges = edges

    # Helper function to calculate total cost of given path in given graph
    # Note that a path needs to be given, not a cycle, we connect the first and last nodes of the path in calculation
    def calculate_cycle_cost(self, path):
        total_cost = 0
        for i in range(len(path) - 1):
            total_cost += self.edges[path[i]][path[i+1]]
        if path[-1] != path[0]:
            total_cost += self.edges[path[-1]][path[0]]
        return total_cost
    
    def convert_to_nxgraph(self) -> nx.Graph:
        nxgraph = nx.Graph()
        #Add all nodes
        for i in range(self.nodes):
            nxgraph.add_node(i)
        #Add all edges and their weights
        for i in range(self.nodes):
            for j in range(i + 1, self.nodes):
                weight = self.edges[i][j]
                if weight != 0:
                    nxgraph.add_edge(i, j, weight=weight)
        return nxgraph
    
    # if initial_pheromones is None, it is set to the mean edge weight
    def convert_to_pherograph(self, initial_pheromones=None):
        if initial_pheromones is None:
            initial_pheromones = self.edges.mean()
        pheromones = np.full_like(self.edges, initial_pheromones, dtype="float64")

        return PheroGraph(self.nodes, self.edges, pheromones)

# graph with pheromones for each edge
# this should be a separate class, since 
#     a. many algorithms don't need pheromones
#     b. different aco algorithms might want to experiment with different initializations
#     c. initializing the pheromones should be part of the time taken by aco (minor)
#     d. running several aco algorithms in sequence might mess up the pheromones (not sure about the python semantics)
class PheroGraph(Graph):
    def __init__(self, nodes, edges, pheromones):
        self.nodes = nodes
        self.edges = edges
        self.pheromones = pheromones

    
#Generate a random complete graph with the specified number of nodes
#The graph will not be metric but it is symmetric
#TODO: add metric graph generation for potential additional comparison with metric TSP algorithms
def generate_random_graph(num_nodes, max_cost=10000):
    edges = np.zeros((num_nodes, num_nodes))

    #Random costs for each pair of nodes -> symmetric
    for i in range(num_nodes):
        for j in range(i + 1, num_nodes):
            cost = random.randint(1, max_cost)
            edges[i][j] = cost
            edges[j][i] = cost

    return Graph(num_nodes, edges)

# This is needed to run TSPLIB95 graphs with the Christophides algorithm (it thinks its a directed graph otherwise)
def convert_nxgraph_to_graph(nxgraph: nx.Graph) -> Graph:
    nodes = nxgraph.number_of_nodes()
    edges = np.zeros((nodes, nodes))

    for i, j, data in nxgraph.edges(data=True):
        edges[i][j] = data['weight']

    return Graph(nodes, edges)

# Returns the TSPLIB95 benchmark graphs, their solutions, and names as networkx, integer, and string arrays respectively
def get_tsplib_graphs():

    filepath = "tsplib95/problems/"
    graphs = []

    for filename in os.listdir(filepath):
        if filename.endswith('.atsp'):
            file = os.path.join(filepath, filename)
            problem = tsplib95.load(file)
            graph = problem.get_graph()
            graphs.append((filename[:-5], graph))

    graphs.sort(key=lambda tup: len(tup[1].nodes()))
    
    # Then we also retrieve the solutions to the respective problems
    df = pd.read_csv("tsplib95/solutions.csv")
    
    optima = {}
    for _, (name, optimum) in df.iterrows():
        optima[name] = optimum

    return graphs, optima