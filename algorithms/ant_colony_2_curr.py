from .algorithm import Algorithm
from graph import Graph, PheroGraph

from typing import List, Tuple
import numpy as np


class Ant_colony_2_curr(Algorithm):
    def __init__(self, 
                 name = "Ant Colony 2 curr",
                 n_ants = 50,
                 n_iterations = 50,
                 alpha = 1,
                 beta = 2,
                 decay_rate = 0.5,
                 elitism = True
                 ):
        self.name = name
        self.n_ants = n_ants
        self.n_iterations = n_iterations
        self.alpha = alpha
        self.beta = beta
        self.decay_rate = decay_rate
        self.elitism = elitism


    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        # initialize pherograph
        self.graph: PheroGraph = graph.convert_to_pherograph(1)

        best_tour = None
        best_cost = 2**60 # hopefully big enough

        # run iterations
        for _ in range(self.n_iterations):
            
            tours = []
            scores = self.compute_scores()

            for _ in range(self.n_ants):
                tour = self.get_tour(scores)
                cost = self.graph.calculate_cycle_cost(tour)
                tours.append((tour, cost))
                if cost < best_cost:
                    best_tour = tour
                    best_cost = cost
            
            self.update_pheromones(tours, best_tour, best_cost)
            
        return best_tour, best_cost
    
    def compute_scores(self):
        scores = np.zeros_like(self.graph.edges)
        n = self.graph.nodes

        for i in range(n):
            for j in range(i+1, n):
                score = self.give_score(i, j)
                scores[i][j] = score
                scores[j][i] = score

        return scores


    def get_tour(self, scores):
        n = self.graph.nodes
        visited = [False for _ in range(n)]
        start = np.random.randint(n)
        path = [start]
        visited[start] = True

        for _ in range(n - 3):
            probabilities = scores[path[-1]].copy()
            probabilities[visited] = 0

            probabilities /= probabilities.sum()

            next = np.random.choice(self.graph.nodes, p=probabilities)
            path.append(next)
            visited[next] = True
        
        last_two = []
        for i in range(n):
            if not visited[i]:
                last_two.append(i)

        r, s, t = path[-1], last_two[0], last_two[1]
        if (self.graph.edges[r][s] + self.graph.edges[t][start] <
            self.graph.edges[r][t] + self.graph.edges[s][start]):
            path += last_two
        else:
            path += [t,s]

        return path

    
    def give_score(self, s, t):
        return self.graph.pheromones[s][t]**self.alpha * (1 / self.graph.edges[s][t])**self.beta


    def update_pheromones(self, tours, best_tour, best_cost):
        # filter out tours and add best
        if self.elitism:
            average_cost = sum([c for _, c in tours]) / len(tours)
            tours = [(t, c) for t, c in tours if c <= average_cost]
            tours.append((best_tour, best_cost))
        
        # decay pheromone
        self.graph.pheromones *= (1 - self.decay_rate)

        # increase pheromone along taken paths
        for tour, cost in tours:
            normalized_inverse_cost = best_cost/cost
            for i in range(-1, len(tour) - 1):
                u, v = tour[i], tour[i+1]
                self.graph.pheromones[u][v] += normalized_inverse_cost
                self.graph.pheromones[v][u] += normalized_inverse_cost
    
    def get_name(self) -> str:
        return self.name
    
    def get_log_string(self) -> str:
        s = f"""{self.name}
        Ants: {self.n_ants}
        Iterations: {self.n_iterations}
        Beta: {self.beta}
        Decay Rate: {self.decay_rate}"""
        return s