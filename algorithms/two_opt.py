from typing import List, Tuple
from .algorithm import Algorithm
from graph import Graph

from tspy import TSP
from tspy.solvers import TwoOpt_solver
import sys
import os


class Two_opt(Algorithm):
    def __init__(self, iter_num=100) -> None:
        self.iter_num = iter_num

    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        # Suppress prints
        original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

        tsp = TSP()
        tsp.read_mat(graph.edges)
        two_opt = TwoOpt_solver(initial_tour='NN', iter_num=self.iter_num)
        two_opt_tour = tsp.get_approx_solution(two_opt)

        # Restore stdout
        sys.stdout = original_stdout
        return two_opt_tour, None

    def get_name(self) -> str:
        return f"Two Opt {self.iter_num}"