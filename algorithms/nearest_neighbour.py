from typing import List, Tuple
from .algorithm import Algorithm
from graph import Graph

from tspy import TSP
from tspy.solvers import NN_solver
import sys
import os


class Nearest_neighbour(Algorithm):
    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        # Suppress prints
        original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

        tsp = TSP()
        tsp.read_mat(graph.edges)
        nn = NN_solver()
        tour = tsp.get_approx_solution(nn)

        # Restore stdout
        sys.stdout = original_stdout
        return tour, None

    def get_name(self) -> str:
        return "NN"