from typing import List, Tuple

import networkx
from .algorithm import Algorithm

class Greedy_nx(Algorithm):
    def find_shortest_cycle(self, nxgraph: networkx.Graph) -> Tuple[List[int], int]:
        return networkx.approximation.greedy_tsp(nxgraph), None
    
    def get_name(self) -> str:
        return "Greedy Nx"
    
    def needs_nx(self) -> bool:
        return True