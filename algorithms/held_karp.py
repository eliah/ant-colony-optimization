import itertools
from typing import List, Tuple
from .algorithm import Algorithm
from graph import Graph

#This is the most well-known exact algorithm. Its a DP solution
class Held_karp(Algorithm):
    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        n = graph.nodes
        #Map every subset of nodes to cost to reach that subset + the path to get there
        C = {}

        #Base case cost (only the first node is in the subset)
        for k in range(1, n):
            C[(1 << k, k)] = (graph.edges[0][k], [0, k])

        #Iterate over subsets with increasing sizes
        #Compute the minimal cost paths
        for subset_size in range(2, n):
            for subset in itertools.combinations(range(1, n), subset_size):
                bits = 0
                for bit in subset:
                    bits |= 1 << bit

                #Find lowest cost to get to this subset
                for k in subset:
                    prev = bits & ~(1 << k)
                    res = []
                    for m in subset:
                        if m == k or graph.edges[m][k] == 0:
                            continue
                        res.append((C[(prev, m)][0] + graph.edges[m][k], m))
                    C[(bits, k)] = min(res)

        #Ignore bits of first node
        bits = (2**n - 1) - 1

        #Calculate minimal cost
        res = []
        for k in range(1, n):
            res.append((C[(bits, k)][0] + graph.edges[k][0], k))
        opt, parent = min(res)

        #Backtrack for optimal path
        path = []
        for _ in range(n - 1):
            path.append(parent)
            new_bits = bits & ~(1 << parent)
            _, parent = C[(bits, parent)]
            bits = new_bits

        #Add start/end
        path.append(0)
        path.reverse()

        return path, opt
    
    def get_name(self):
        return "Held-Karp"