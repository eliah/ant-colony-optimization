from typing import List, Tuple
import networkx
from .algorithm import Algorithm

class Christofides(Algorithm):
    def find_shortest_cycle(self, nxgraph: networkx.Graph) -> Tuple[List[int], int]:
        return networkx.approximation.christofides(nxgraph), None
    
    def get_name(self) -> str:
        return "Christofides"
    
    def needs_nx(self) -> bool:
        return True