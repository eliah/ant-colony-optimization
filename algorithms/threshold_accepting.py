from typing import List, Tuple
import networkx
from .algorithm import Algorithm

class Threshold_accepting(Algorithm):
    # Default values are used for all parameters
    # An initial cycle is needed and can be either greedy or all nodes numbers in ascending order
    # (both initializations are suggestions by the networkx documentation)
    def find_shortest_cycle(self, nxgraph: networkx.Graph) -> Tuple[List[int], int]:
        initial_cycle = list(nxgraph) + [next(iter(nxgraph))]
        # initial_cycle = networkx.approximation.greedy_tsp(nxgraph)
        optimal_path = networkx.algorithms.approximation.threshold_accepting_tsp(nxgraph, initial_cycle)
        return optimal_path, None
    
    def get_name(self) -> str:
        return "Threshold Accepting"
    
    def needs_nx(self) -> bool:
        return True