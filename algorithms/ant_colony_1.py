import random
from typing import List, Tuple
import numpy as np

from graph import Graph
from .algorithm import Algorithm

class Ant_colony_1(Algorithm):
    def __init__(self, iterations = 100, 
                 ants_per_iteration=50, 
                 alpha=1.0, 
                 beta=2.0, 
                 evaporation_rate=0.5, 
                 q=100.0,
                 name="Ant Colony 1"):
        self.iterations = iterations
        self.ants_per_iteration = ants_per_iteration
        self.alpha = alpha
        self.beta = beta
        self.evaporation_rate = evaporation_rate
        self.q = q
        self.name = name

    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        graph = graph.convert_to_pherograph()
        best_path = None
        best_cost = float("inf")

        for _ in range(self.iterations):
            for _ in range(self.ants_per_iteration):
                path = [random.randint(0, graph.nodes - 1)]
                for _ in range(graph.nodes - 1):
                    current = path[-1]
                    probabilities = []
                    for j in range(graph.nodes):
                        if j not in path:
                            pheromone = graph.pheromones[current][j] ** self.alpha
                            desirability = (1.0 / graph.edges[current][j]) ** self.beta
                            probabilities.append(pheromone * desirability)
                        else:
                            probabilities.append(0)
                    probabilities = np.array(probabilities)
                    probabilities /= probabilities.sum()
                    next_city = np.random.choice(graph.nodes, p=probabilities)
                    path.append(next_city)

                cost = graph.calculate_cycle_cost(path)
                if cost < best_cost:
                    best_cost = cost
                    best_path = path

                for i in range(len(path) - 1):
                    graph.pheromones[path[i]][path[i + 1]] += self.q / cost
                graph.pheromones[path[-1]][path[0]] += self.q / cost

            graph.pheromones *= (1 - self.evaporation_rate)

        return best_path, best_cost
    
    def get_name(self) -> str:
        return self.name