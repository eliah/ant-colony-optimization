from typing import List, Tuple

import networkx
from .algorithm import Algorithm
from networkx import Graph as NxGraph

class Simulated_annealing(Algorithm):
    def find_shortest_cycle(self, nxgraph: NxGraph) -> Tuple[List[int], int]:
        initial_cycle = list(nxgraph) + [next(iter(nxgraph))]
        # initial_cycle = networkx.approximation.greedy_tsp(nxgraph)
        optimal_path = networkx.algorithms.approximation.simulated_annealing_tsp(nxgraph, initial_cycle)
        optimal_path = optimal_path[0:(len(optimal_path) - 1)]
        return optimal_path, None
    
    def get_name(self) -> str:
        return "Simulated Annealing"
    
    def needs_nx(self) -> bool:
        return True