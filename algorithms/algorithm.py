from graph import Graph
from typing import List, Tuple

# interface for any algorithm that solves TSP
class Algorithm:
    # for algorithms based on networkx, the cost returned is null
    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        pass

    def get_name(self) -> str:
        pass

    def get_log_string(self) -> str:
        return self.get_name()

    def needs_nx(self) -> bool:
        return False
    