import itertools
from typing import List, Tuple
from .algorithm import Algorithm
from graph import Graph

class Brute_force(Algorithm):
    def find_shortest_cycle(self, graph: Graph) -> Tuple[List[int], int]:
        num_nodes = graph.nodes
        best_path = None
        best_cost = float('inf')

        #All possible permutations of node indices (except start node)
        node_indices = list(range(1, num_nodes))
        permutations = itertools.permutations(node_indices)

        #Iterate through all permutations, calculate total cost for each path
        for perm in permutations:
            #Add back start node since we removed that from the permutations
            path = [0] + list(perm)
            cost = graph.calculate_cycle_cost(path)

            #Update best path and cost if current path is shorter
            if cost < best_cost:
                best_cost = cost
                best_path = path

        return best_path, best_cost
    
    def get_name(self):
        return "Brute force"