import networkx.algorithms.approximation
import numpy as np
import random
import itertools
import networkx as nx
import matplotlib.pyplot as plt
import time


"""
This is the python code behind everything in this project (my ACO implementation to solve TSP, all other TSP algorithms,
the visualization of all the graphs, and the the timings recorded for all of them).
I tried to comment everything to make it as understandable as possible even for people not too familiar with python.
"""

#Custom graph implementation that includes pheromone levels for every edge
#Nodes is number of nodes
#Edges is a 2d symmetric matrix, entry [i,j] is the cost of edge between node i to node j (or j to i)
#Pheromones is also a 2d symmetric matrix, entry [i,j] is the pheromone level of edge between node i to node j (or j to i)
class Graph():
    def __init__(self, nodes, edges, pheromones):
        self.nodes = nodes
        self.edges = edges
        self.pheromones = pheromones


#Generate a random complete graph with the specified number of nodes
#The graph will not be metric but it is symmetric
#TODO: add metric graph generation for potential additional comparison with metric TSP algorithms
def generate_complete_graph(num_nodes, max_cost):
    edges = np.zeros((num_nodes, num_nodes))

    #Random costs for each pair of nodes -> symmetric
    for i in range(num_nodes):
        for j in range(i + 1, num_nodes):
            cost = random.randint(1, max_cost)
            edges[i][j] = cost
            edges[j][i] = cost

    #We also always have to initialize pheromones
    #The value we use for this is a bit arbitrary, with large enough max_cost we can do the mean
    #TODO: experiment with this value
    init_value = edges.mean()
    pheromones = np.full_like(edges, init_value, dtype="float64")

    return Graph(num_nodes, edges, pheromones)


#Visualize given graph with networkx + matplotlib
#Can be given solution path, which will be shown with red edges
#Given path should actually be the solution, so either from brute force or another non-approximate algorithm
#solution_only boolean denotes if all edges should be shown or only the solution (if provided)
def visualize_graph(graph, path=None, solution_only=False, title_algo=""):
    #Convert custom graph to networkx graph
    G = nx.Graph()

    #Add all nodes
    for i in range(graph.nodes):
        G.add_node(i)

    #Add all edges and their weights
    if not solution_only or path is None:
        for i in range(graph.nodes):
            for j in range(i + 1, graph.nodes):
                weight = graph.edges[i][j]
                if weight != 0:
                    G.add_edge(i, j, weight=weight)

    #Add edges of solution path (if given)
    solution_edges = []
    if path is not None:
        for i in range(len(path) - 1):
            G.add_edge(path[i], path[i + 1], weight=graph.edges[path[i]][path[i + 1]])
            solution_edges.append((path[i], path[i + 1]))
        solution_edges.append((path[0], path[len(path) - 1]))

    #Automatically create a nice layout for the nodes
    #We set a seed to have the same layout for solutions of different algorithms
    #If we did not set seed, making visual comparissions would be harder
    pos = nx.spring_layout(G, seed=1234)

    #Draw all the components (nodes, edges, labels)
    nx.draw_networkx_nodes(G, pos, node_size=700)
    if not solution_only or path is None:
        nx.draw_networkx_edges(G, pos, width=1.0, alpha=0.5)
    if path is not None:
        nx.draw_networkx_edges(G, pos, edgelist=solution_edges, width=2.0, edge_color='r')
    edge_labels = {(i, j): G[i][j]['weight'] for i, j in G.edges()}
    nx.draw_networkx_edge_labels(G, pos, edge_labels=edge_labels, font_color='red')
    nx.draw_networkx_labels(G, pos, font_size=20, font_family="sans-serif")

    # Show graph in new window to user
    plt.axis("off")
    if path is not None:
        plt.title("Graph with TSP Solution using " + title_algo)
    else:
        plt.title("Complete Graph")
    plt.show()


#Helper function to calculate total cost of given path in given graph
#Note that a path needs to be given, not a cycle, we connect the first and last nodes of the path in calculation
def calculate_cycle_cost(graph, path):
    total_cost = 0
    for i in range(len(path) - 1):
        total_cost += graph.edges[path[i]][path[i+1]]
    total_cost += graph.edges[path[-1]][path[0]]
    return total_cost


#Brute force TSP solution for comparisson
#This is obviously very slow!
#Dont use on graphs much larger than 8 or so, will take really long!
def brute_force_tsp(graph):
    num_nodes = graph.nodes
    best_path = None
    best_cost = float('inf')

    #All possible permutations of node indices (except start node)
    node_indices = list(range(1, num_nodes))
    permutations = itertools.permutations(node_indices)

    #Iterate through all permutations, calculate total cost for each path
    for perm in permutations:
        #Add back start node since we removed that from the permutations
        path = [0] + list(perm)
        cost = calculate_cycle_cost(graph, path)

        #Update best path and cost if current path is shorter
        if cost < best_cost:
            best_cost = cost
            best_path = path

    return best_path, best_cost


#TODO: not fully optimized, compare with wikipedia again. The combinations could be done different (but more complex)
#This is the most well-known exact algorithm. Its a DP solution
def held_karp(graph):
    n = graph.nodes
    #Map every subset of nodes to cost to reach that subset + the path to get there
    C = {}

    #Base case cost (only the first node is in the subset)
    for k in range(1, n):
        C[(1 << k, k)] = (graph.edges[0][k], [0, k])

    #Iterate over subsets with increasing sizes
    #Compute the minimal cost paths
    for subset_size in range(2, n):
        for subset in itertools.combinations(range(1, n), subset_size):
            bits = 0
            for bit in subset:
                bits |= 1 << bit

            #Find lowest cost to get to this subset
            for k in subset:
                prev = bits & ~(1 << k)
                res = []
                for m in subset:
                    if m == k or graph.edges[m][k] == 0:
                        continue
                    res.append((C[(prev, m)][0] + graph.edges[m][k], m))
                C[(bits, k)] = min(res)

    #Ignore bits of first node
    bits = (2**n - 1) - 1

    #Calculate minimal cost
    res = []
    for k in range(1, n):
        res.append((C[(bits, k)][0] + graph.edges[k][0], k))
    opt, parent = min(res)

    #Backtrack for optimal path
    path = []
    for i in range(n - 1):
        path.append(parent)
        new_bits = bits & ~(1 << parent)
        _, parent = C[(bits, parent)]
        bits = new_bits

    #Add start/end
    path.append(0)
    path.reverse()

    return path, opt


def ant_colony_optimization(graph, iterations=100, ants_per_iteration=50, alpha=1.0, beta=2.0, evaporation_rate=0.5, q=100.0):
    best_path = None
    best_cost = float("inf")

    for _ in range(iterations):
        for _ in range(ants_per_iteration):
            path = [random.randint(0, graph.nodes - 1)]
            for _ in range(graph.nodes - 1):
                current = path[-1]
                probabilities = []
                for j in range(graph.nodes):
                    if j not in path:
                        pheromone = graph.pheromones[current][j] ** alpha
                        desirability = (1.0 / graph.edges[current][j]) ** beta
                        probabilities.append(pheromone * desirability)
                    else:
                        probabilities.append(0)
                probabilities = np.array(probabilities)
                probabilities /= probabilities.sum()
                next_city = np.random.choice(graph.nodes, p=probabilities)
                path.append(next_city)

            cost = calculate_cycle_cost(graph, path)
            if cost < best_cost:
                best_cost = cost
                best_path = path

            for i in range(len(path) - 1):
                graph.pheromones[path[i]][path[i + 1]] += q / cost
            graph.pheromones[path[-1]][path[0]] += q / cost

        graph.pheromones *= (1 - evaporation_rate)

    return best_path, best_cost


def run_benchmark(graph, nxgraph, do_brute_force=False, do_asadpour=False, do_held_karp=True):
    times = [-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
    costs = [-1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0]
    index = 0

    # Solve TSP using brute force
    if do_brute_force:
        brute_force_time_start = time.time()
        optimal_path, optimal_cost = brute_force_tsp(graph)
        brute_force_time = time.time() - brute_force_time_start
        times[index] = brute_force_time
        costs[index] = optimal_cost
    index += 1

    # Solve TSP using Held-Karp
    if do_held_karp:
        held_karp_time_start = time.time()
        optimal_path, optimal_cost = held_karp(graph)
        held_karp_time = time.time() - held_karp_time_start
        times[index] = held_karp_time
        costs[index] = optimal_cost
    index += 1

    # --Solve with networkx solvers--
    #
    # -Greedy-
    greedy_time_start = time.time()
    optimal_path = networkx.approximation.greedy_tsp(nxgraph)
    greedy_time = time.time() - greedy_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    times[index] = greedy_time
    costs[index] = optimal_cost
    index += 1
    #
    # -Christofides-
    christ_time_start = time.time()
    optimal_path = networkx.approximation.christofides(nxgraph)
    christ_time = time.time() - christ_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    times[index] = christ_time
    costs[index] = optimal_cost
    index += 1
    #
    # -Simulated Annealing-
    # Default values are used for all parameters
    # An initial cycle is needed and can be either greedy or all nodes numbers in ascending order
    # (both initializations are suggestions by the networkx documentation)
    sa_time_start = time.time()
    initial_cycle = list(nxgraph) + [next(iter(nxgraph))]
    # initial_cycle = networkx.approximation.greedy_tsp(nxgraph)
    optimal_path = networkx.algorithms.approximation.simulated_annealing_tsp(nxgraph, initial_cycle)
    sa_time = time.time() - sa_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    times[index] = sa_time
    costs[index] = optimal_cost
    index += 1
    #
    # -Threshold Accepting-
    # Default values are used for all parameters
    # An initial cycle is needed and can be either greedy or all nodes numbers in ascending order
    # (both initializations are suggestions by the networkx documentation)
    ta_time_start = time.time()
    initial_cycle = list(nxgraph) + [next(iter(nxgraph))]
    # initial_cycle = networkx.approximation.greedy_tsp(nxgraph)
    optimal_path = networkx.algorithms.approximation.threshold_accepting_tsp(nxgraph, initial_cycle)
    ta_time = time.time() - ta_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    times[index] = ta_time
    costs[index] = optimal_cost
    index += 1
    #
    # -Asadpour-
    # One of the best approximate ASYMMETRIC TSP solvers
    # I initially hoped it would also perform well for symmetric TSP but that is not the case
    # It takes multiple minutes even with <10 nodes but we cant blame it for this as it was not designed for symmetric graphs
    if do_asadpour:
        # Since we are working with symmetric undirected graphs we first have to convert it to a directed graph
        # Edges will still be symmetric
        DG = nx.DiGraph()
        # Add all nodes
        for i in range(graph.nodes):
            DG.add_node(i)
        # Add all edges and their weights
        for i in range(graph.nodes):
            for j in range(i + 1, graph.nodes):
                weight = graph.edges[i][j]
                if weight != 0:
                    DG.add_edge(i, j, weight=weight)
                    DG.add_edge(j, i, weight=weight)
        #
        asad_time_start = time.time()
        optimal_path = networkx.algorithms.approximation.asadpour_atsp(DG)
        asad_time = time.time() - asad_time_start
        optimal_path = optimal_path[0:(len(optimal_path) - 1)]
        optimal_cost = calculate_cycle_cost(graph, optimal_path)
        times[index] = asad_time
        costs[index] = optimal_cost
    index += 1

    # Solve TSP using ACO
    aco_time_start = time.time()
    optimal_path, optimal_cost = ant_colony_optimization(graph)
    aco_time = time.time() - aco_time_start
    times[index] = aco_time
    costs[index] = optimal_cost

    return times, costs


#Benchmark helper function to more easily see which algorithm is what line (I'm somewhat colorblind and cant identify them)
def add_labels(ax, x_data, y_data, labels):
    for i, (x, y, label) in enumerate(zip(x_data, y_data, labels)):
        if x and y:
            ax.text(x[-1], y[-1], label, fontsize=9, ha='left', va='center')


#Main function used to benchmark all the algorithms.
#For every step in node_counts it creates num_graphs amount of graphs and then runs every algorithm on them.
#It then compares the resulting costs (and the mean of all of them) to the optimal solution.
#It saves the runtime of every algorithm for every graph and also saves them (and a mean).
#If a solution has the same cost as the optimal solution then that is also noted.
#Lastly we tabulate the times and costs.
def benchmark(do_held_karp=True):
    num_graphs = 3
    node_counts = [5, 10, 20]
    mean_times = []
    mean_costs = []
    optimals = []

    algo_names = {
        0: "Brute Force",
        1: "Held-Karp",
        2: "Greedy",
        3: "Christofides",
        4: "Simulated Annealing",
        5: "Threshold Accepting",
        6: "Asadpour",
        7: "Ant Colony"
    }

    for num_nodes in node_counts:
        total_times = [0] * 8
        total_costs = [0] * 8
        total_optimal = [0] * 8
        total_optimal[0] = num_graphs
        for _ in range(num_graphs):
            #Generate graph
            graph = generate_complete_graph(num_nodes, 100)

            #Convert to networkx graph
            nxgraph = nx.Graph()
            #Add all nodes
            for i in range(graph.nodes):
                nxgraph.add_node(i)
            #Add all edges and their weights
            for i in range(graph.nodes):
                for j in range(i + 1, graph.nodes):
                    weight = graph.edges[i][j]
                    if weight != 0:
                        nxgraph.add_edge(i, j, weight=weight)

            #Run all algos on this graph
            times, costs = run_benchmark(graph, nxgraph, False, False, do_held_karp)
            for i in range(8):
                total_times[i] += times[i]
                total_costs[i] += costs[i]
                if do_held_karp and i >= 1:
                    if costs[i] == costs[1]:
                        total_optimal[i] += 1


        for i in range(8):
            total_times[i] /= num_graphs
            total_costs[i] /= num_graphs

        #Compute mean of times and cost over all graphs with this size for every algorithm
        mean_times.append(total_times)
        mean_costs.append(total_costs)
        if do_held_karp:
            optimals.append(total_optimal)

    #Plot the results
    #
    #TODO: Look into doing linlog plot here
    #First we plot the mean time over all node_counts
    fig, ax = plt.subplots(figsize=(12, 6))
    labels = []
    x_data = []
    y_data = []
    for i in range(8):
        times = [mean_time[i] for mean_time in mean_times if mean_time[i] != -1.0]
        counts = [node_counts[j] for j in range(len(node_counts)) if mean_times[j][i] != -1.0]
        if times:
            algo = algo_names[i]
            ax.plot(counts, times, label=algo)
            labels.append(algo)
            x_data.append(counts)
            y_data.append(times)
    #We also add labels on each algos line directly if someone cant identify the colors (like myself)
    add_labels(ax, x_data, y_data, labels)

    plt.xlabel("Number of Nodes")
    plt.ylabel("Mean Time")
    plt.title("Mean Time vs Number of Nodes")
    plt.legend()
    plt.grid(True)
    plt.show()

    #TODO: Look into doing a second graph where we plot optimal_cost - cost
    #Then we plot the mean costs
    fig, ax = plt.subplots(figsize=(12, 6))
    labels = []
    x_data = []
    y_data = []
    for i in range(8):
        costs = [mean_cost[i] for mean_cost in mean_costs if mean_cost[i] != -1.0]
        counts = [node_counts[j] for j in range(len(node_counts)) if mean_costs[j][i] != -1.0]
        if costs:
            algo = algo_names[i]
            ax.plot(counts, costs, label=algo)
            labels.append(algo)
            x_data.append(counts)
            y_data.append(costs)

    add_labels(ax, x_data, y_data, labels)

    plt.xlabel("Number of Nodes")
    plt.ylabel("Mean Cost")
    plt.title("Mean Cost vs Number of Nodes")
    plt.legend()
    plt.grid(True)
    plt.show()

    #TODO: Look into also doing MSE here, or second graph with MSE
    #Here we plot how many times each algo matches the optimal
    if do_held_karp:
        fig, ax = plt.subplots(figsize=(12, 6))
        labels = []
        x_data = []
        y_data = []
        for i in range(2, 8):
            opt = [optimal[i] for optimal in optimals if optimal[i] != -1.0]
            counts = [node_counts[j] for j in range(len(node_counts)) if optimals[j][i] != -1.0]
            if opt:
                algo = algo_names[i]
                ax.plot(counts, opt, label=algo)
                labels.append(algo)
                x_data.append(counts)
                y_data.append(costs)

        add_labels(ax, x_data, y_data, labels)

        plt.xlabel("Number of Nodes")
        plt.ylabel("Optimal Matches")
        plt.title("Optimal Matches vs Number of Nodes")
        plt.legend()
        plt.grid(True)
        plt.show()

    #We now try to show the tradeoff between speed and accuracy
    #First we take the amount of optimal matches as the measure for accuracy
    #This is likely the second most important graph of this project
    if do_held_karp:
        overall_mean_times = []
        optimal_match_percentages = []

        for i in range(8):
            valid_mean_times = [mean_time[i] for mean_time in mean_times if mean_time[i] != -1.0]
            if valid_mean_times:
                mean_time = sum(valid_mean_times) / len(valid_mean_times)
                overall_mean_times.append(mean_time)
                total_optimal_matches = sum(optimal[i] for optimal in optimals)
                optimal_percentage = total_optimal_matches / (num_graphs * len(node_counts)) * 100
                optimal_match_percentages.append(optimal_percentage)
            else:
                overall_mean_times.append(-1.0)
                optimal_match_percentages.append(-1.0)

        #We do this with a scatter plot
        fig, ax = plt.subplots(figsize=(12, 6))
        for i in range(8):
            if overall_mean_times[i] != -1.0:
                ax.scatter(overall_mean_times[i], optimal_match_percentages[i], label=algo_names[i])
                ax.text(overall_mean_times[i], optimal_match_percentages[i], algo_names[i], fontsize=9, ha="left", va="bottom")

        plt.xlabel("Mean Time")
        plt.ylabel("Optimal Match Percentage")
        plt.title("Tradeoff Between Accuracy and Speed")
        plt.legend()
        plt.grid(True)
        plt.show()

        #We show the same tradeoff but with the mean squared error of costs as accuracy
        #This is likely the most important graph of this project
        overall_mean_times = []
        mse_values = []

        for i in range(8):
            valid_mean_times = [mean_time[i] for mean_time in mean_times if mean_time[i] != -1.0]
            if valid_mean_times:
                mean_time = sum(valid_mean_times) / len(valid_mean_times)
                overall_mean_times.append(mean_time)

                #We calculate the MSE with the Held-Karp (in index 1) as ground truth
                mse = 0.0
                count = 0
                for j in range(len(node_counts)):
                    if mean_costs[j][i] != -1.0 and mean_costs[j][1] != -1.0:
                        mse += (mean_costs[j][i] - mean_costs[j][1]) ** 2
                        count += 1
                mse /= count
                mse_values.append(mse)
            else:
                overall_mean_times.append(-1.0)
                mse_values.append(-1.0)

        fig, ax = plt.subplots(figsize=(12, 6))
        for i in range(8):
            if overall_mean_times[i] != -1.0:
                ax.scatter(overall_mean_times[i], mse_values[i], label=algo_names[i])
                ax.text(overall_mean_times[i], mse_values[i], algo_names[i], fontsize=9, ha="left", va="bottom")

        plt.xlabel("Mean Time")
        plt.ylabel("Mean Squared Error (MSE)")
        plt.title("Tradeoff Between Accuracy (MSE) and Speed")
        plt.legend()
        plt.grid(True)
        plt.show()


#Runs all the algorithms possible (except those specified not to) on a graph with the specified number of nodes.
#The results are printed to console
def single_test(num_nodes=10, do_brute_force=False, do_asadpour=False, do_held_karp=True, show_visualisations=False):

    #num_nodes: How many nodes the generated graph should have
    #do_brute_force: Not recommended on graphs with >10 nodes
    #do_asadpour: Not recommended, really slow on symmetric graphs (was made for asymmetric graphs)
    #do_held_karp: Finds exact solution, slow on graphs with >20 nodes
    #show_visualisations: Whether we show the graphs and solutions or not

    graph = generate_complete_graph(num_nodes, 100)
    if show_visualisations:
        visualize_graph(graph)

    # Solve TSP using brute force
    if do_brute_force:
        brute_force_time_start = time.time()
        optimal_path, optimal_cost = brute_force_tsp(graph)
        brute_force_time = time.time() - brute_force_time_start
        print()
        print("Optimal Cycle:", optimal_path)
        print("Optimal Cost:", optimal_cost)
        print("Execution time Brute Force:", brute_force_time, "seconds")
        if show_visualisations:
            visualize_graph(graph, optimal_path, False, "Brute Force")

    # Solve TSP using Held-Karp
    if do_held_karp:
        held_karp_time_start = time.time()
        optimal_path, optimal_cost = held_karp(graph)
        held_karp_time = time.time() - held_karp_time_start
        print()
        print("Optimal Cycle:", optimal_path)
        print("Optimal Cost:", optimal_cost)
        print("Execution time Held-Karp:", held_karp_time, "seconds")
        if show_visualisations:
            visualize_graph(graph, optimal_path, False, "Held-Karp")

    # --Solve with networkx solvers--
    #
    # We first have to convert to a networkx graph
    G = nx.Graph()
    # Add all nodes
    for i in range(graph.nodes):
        G.add_node(i)
    # Add all edges and their weights
    for i in range(graph.nodes):
        for j in range(i + 1, graph.nodes):
            weight = graph.edges[i][j]
            if weight != 0:
                G.add_edge(i, j, weight=weight)
    #
    # -Greedy-
    greedy_time_start = time.time()
    optimal_path = networkx.approximation.greedy_tsp(G)
    greedy_time = time.time() - greedy_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    print()
    print("Cycle:", optimal_path)
    print("Cost:", optimal_cost)
    print("Execution time Greedy:", greedy_time, "seconds")
    if show_visualisations:
        visualize_graph(graph, optimal_path, False, "Greedy")
    #
    # -Christofides-
    christ_time_start = time.time()
    optimal_path = networkx.approximation.christofides(G)
    christ_time = time.time() - christ_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    print()
    print("Cycle:", optimal_path)
    print("Cost:", optimal_cost)
    print("Execution time Christofides:", christ_time, "seconds")
    if show_visualisations:
        visualize_graph(graph, optimal_path, False, "Christofides")
    #
    # -Simulated Annealing-
    # Default values are used for all parameters
    # An initial cycle is needed and can be either greedy or all nodes numbers in ascending order
    # (both initializations are suggestions by the networkx documentation)
    sa_time_start = time.time()
    initial_cycle = list(G) + [next(iter(G))]
    # initial_cycle = networkx.approximation.greedy_tsp(G)
    optimal_path = networkx.algorithms.approximation.simulated_annealing_tsp(G, initial_cycle)
    sa_time = time.time() - sa_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    print()
    print("Cycle:", optimal_path)
    print("Cost:", optimal_cost)
    print("Execution time Simulated Annealing:", sa_time, "seconds")
    if show_visualisations:
        visualize_graph(graph, optimal_path, False, "Simulated Annealing")
    #
    # -Threshold Accepting-
    # Default values are used for all parameters
    # An initial cycle is needed and can be either greedy or all nodes numbers in ascending order
    # (both initializations are suggestions by the networkx documentation)
    ta_time_start = time.time()
    initial_cycle = list(G) + [next(iter(G))]
    # initial_cycle = networkx.approximation.greedy_tsp(G)
    optimal_path = networkx.algorithms.approximation.threshold_accepting_tsp(G, initial_cycle)
    ta_time = time.time() - ta_time_start
    optimal_path = optimal_path[0:(len(optimal_path) - 1)]
    optimal_cost = calculate_cycle_cost(graph, optimal_path)
    print()
    print("Cycle:", optimal_path)
    print("Cost:", optimal_cost)
    print("Execution time Threshold Accepting:", ta_time, "seconds")
    if show_visualisations:
        visualize_graph(graph, optimal_path, False, "Threshold Accepting")
    #
    # -Asadpour-
    # One of the best approximate ASYMMETRIC TSP solvers
    # I initially hoped it would also perform well for symmetric TSP but that is not the case
    # It takes multiple minutes even with <10 nodes but we cant blame it for this as it was not designed for symmetric graphs
    if do_asadpour:
        # Since we are working with symmetric undirected graphs we first have to convert it to a directed graph
        # Edges will still be symmetric
        DG = nx.DiGraph()
        # Add all nodes
        for i in range(graph.nodes):
            DG.add_node(i)
        # Add all edges and their weights
        for i in range(graph.nodes):
            for j in range(i + 1, graph.nodes):
                weight = graph.edges[i][j]
                if weight != 0:
                    DG.add_edge(i, j, weight=weight)
                    DG.add_edge(j, i, weight=weight)
        #
        asad_time_start = time.time()
        optimal_path = networkx.algorithms.approximation.asadpour_atsp(DG)
        asad_time = time.time() - asad_time_start
        optimal_path = optimal_path[0:(len(optimal_path) - 1)]
        optimal_cost = calculate_cycle_cost(graph, optimal_path)
        print()
        print("Cycle:", optimal_path)
        print("Cost:", optimal_cost)
        print("Execution time Asadpour:", asad_time, "seconds")
        if show_visualisations:
            visualize_graph(graph, optimal_path, False, "Asadpour")

    # Solve TSP using ACO
    aco_time_start = time.time()
    optimal_path, optimal_cost = ant_colony_optimization(graph)
    aco_time = time.time() - aco_time_start
    print()
    print("Cycle:", optimal_path)
    print("Cost:", optimal_cost)
    print("Execution time ACO:", aco_time, "seconds")
    if show_visualisations:
        visualize_graph(graph, optimal_path, False, "ACO")

if __name__ == '__main__':
    #single_test(10)
    benchmark(True)





#TODO: Look into concorde tsp solver for an additional exact algorithm
#TODO: Look into "math twice and stitch", "Lin-kernighan", "nearest neighbor", for heuristic alrogithms
#https://www.baeldung.com/cs/tsp-exact-solutions-vs-heuristic-vs-approximation-algorithms
