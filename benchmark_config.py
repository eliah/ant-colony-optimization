# config file for the benchmark
from algorithms.greedy_nx import Greedy_nx
from algorithms.simulated_annealing import Simulated_annealing
from algorithms.christofides import Christofides
from algorithms.threshold_accepting import Threshold_accepting
from algorithms.held_karp import Held_karp
from algorithms.brute_force import Brute_force
from algorithms.ant_colony_1 import Ant_colony_1
from algorithms.ant_colony_2_original import Ant_colony_2_original
from algorithms.ant_colony_2_fastest import Ant_colony_2_fastest
from algorithms.ant_colony_2_curr import Ant_colony_2_curr
from algorithms.ant_colony_2_adaptive import Ant_colony_2_adaptive
from algorithms.ant_colony_2_adaptive_2 import Ant_colony_2_adaptive_2
from algorithms.nearest_neighbour import Nearest_neighbour
from algorithms.two_opt import Two_opt

# ALGORITHMS = [ # benchmark is run on every algorithm in this list
#     # Greedy_nx(),
#     # Simulated_annealing(),
#     # Christofides(),
#     # Threshold_accepting(),
#     # Nearest_neighbour(),
#     # Two_opt(),
#     # Two_opt(10),
#     # Ant_colony_1(),
#     # Ant_colony_2_original(),
#     # Ant_colony_2_fastest(),
#     # Ant_colony_2_adaptive(threshold=2),
#     # Ant_colony_2_adaptive(threshold=1),
#     # Ant_colony_2_adaptive_2(),
#     Ant_colony_2_adaptive_2(threshold=5),
#     Ant_colony_2_adaptive_2(name="more decay", threshold=5, decay_rate=0.8),
#     Ant_colony_2_adaptive_2(name="less decay", threshold=5, decay_rate=0.2),
#     # Ant_colony_2_adaptive_2(name = "ACO linear 50",min_ants=50, ant_exponent=1),
#     # Ant_colony_2_adaptive_2(name = "ACO",min_ants=30, threshold=10),
#     # Ant_colony_2_adaptive_2(name = "ACO linear 50",min_ants=50, ant_exponent=1, threshold=10),
#     # Ant_colony_2_adaptive_2(name = "ACO linear", ant_exponent=1),
#     # Ant_colony_2_curr()
# ]

ALGORITHMS = [
    Greedy_nx(),
    Simulated_annealing(),
    Threshold_accepting(),
    Nearest_neighbour(),
    Two_opt(),
    Ant_colony_2_adaptive_2(),

]

max_size = 3000
step = 1000

GRAPH_SIZES = [ # benchmark is run for every size of graph in this list
    i for i in range(step, max_size+1, step)
]

NUMBER_OF_RUNS = 3 # number of runs per graph size

TSPLIB = True # whether benchmark graphs are randomly generated or taken from tsplib