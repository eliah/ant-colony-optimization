from datetime import datetime
import time

def time_string():
    now = datetime.now()
    miliseconds = now.microsecond // 1000
    return now.strftime(f"[%H:%M:%S.{miliseconds:03d}]")

def printt(s):
    print(time_string(), s)

def print_finished(start):
    elapsed = int(time.time() - start)
    mins = elapsed // 60
    secs = elapsed % 60
    printt(f"Finished in {mins}:{secs:02d}")