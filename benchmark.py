import time
from typing import Tuple
from algorithms.algorithm import Algorithm
from graph import Graph, generate_random_graph, get_tsplib_graphs, convert_nxgraph_to_graph
from networkx import Graph as NxGraph
from benchmark_config import ALGORITHMS, GRAPH_SIZES, NUMBER_OF_RUNS, TSPLIB
from file_setup import CSV_PATH, LOG_PATH
from helpers import printt
import csv

# returns the time algorithm takes and the cost of the returned cycle
def single_test(algorithm: Algorithm, graph: Graph, nxgraph: NxGraph) -> Tuple[float, int]:
    input_graph = nxgraph if algorithm.needs_nx() else graph
    start = time.time()
    best_path, path_cost = algorithm.find_shortest_cycle(input_graph)
    time_elapsed = time.time() - start
    if path_cost is None:
        path_cost = graph.calculate_cycle_cost(best_path)
    return time_elapsed, path_cost

def run_benchmark() -> None:
    if TSPLIB:
        printt("Running tsplib benchmark")
        run_tsplib_benchmark()
    else:
        printt("Running own benchmark")
        run_own_benchmark()
    printt("Benchmark finished")

# runs benchmark with the parameters specified above
def run_own_benchmark() -> None:
    write_log()
    
    header = ["Algorithm", "Size", "Run", "Time", "Cost"]
    with open(CSV_PATH, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(header)
    printt(f"Created {CSV_PATH}")

    needs_nx = False
    for algorithm in ALGORITHMS:
        needs_nx = needs_nx or algorithm.needs_nx()

    # Benchmarking on randomly generated graphs
    for size in GRAPH_SIZES:
        printt(f"Starting with size {size}")
        for run in range(NUMBER_OF_RUNS):
            graph = generate_random_graph(size)
            nxgraph = graph.convert_to_nxgraph() if needs_nx else None
            for algorithm in ALGORITHMS:
                t, c = single_test(algorithm, graph, nxgraph)
                with open(CSV_PATH, 'a', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow([algorithm.get_name(), size, run, t, c])


def run_tsplib_benchmark():
    write_log()
    
    header = ["Algorithm", "Size", "Run", "Time", "Cost"]
    with open(CSV_PATH, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(header)
    printt(f"Created {CSV_PATH}")

    # Benchmarking on TSPLIB95 graphs
    graphs, optima = get_tsplib_graphs()
    for name, nxgraph in graphs:
        printt(f"Starting with {name}")
        graph = convert_nxgraph_to_graph(nxgraph)
        for run in range(NUMBER_OF_RUNS):
            for algorithm in ALGORITHMS:
                t, c = single_test(algorithm, graph, nxgraph)
                with open(CSV_PATH, 'a', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow([algorithm.get_name(), name, run, t, c])
            # write optimal solution
            with open(CSV_PATH, 'a', newline='') as file:
                writer = csv.writer(file)
                writer.writerow(["Optimum", name, run, 0, optima[name]])


def write_log():
    with open(LOG_PATH, 'w') as file:
        if not TSPLIB:
            file.write(f"Graph sizes: {', '.join(map(str, GRAPH_SIZES))}\n")
            file.write(f"Number of runs: {NUMBER_OF_RUNS}\n\n")

        file.write("Algorithms\n")
        for alg in ALGORITHMS:
            file.write(f"{alg.get_log_string()}\n")

    printt(f"Saved {LOG_PATH}")