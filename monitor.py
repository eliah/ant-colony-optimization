from algorithms.ant_colony_2_monitor import Ant_colony_2_mon
from graph import Graph, generate_random_graph
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from helpers import printt

GRAPH_SIZE = 400
ITERATIONS = 100
RUNS = 20

data = []

for i in range(RUNS):
    printt(f"Starting run {i}")
    graph = generate_random_graph(GRAPH_SIZE)
    algorithm = Ant_colony_2_mon(n_iterations=ITERATIONS)
    data.append(algorithm.find_shortest_cycle(graph))

printt("Starting analysis")

matrix = np.array(data)
for row in matrix:
    row /= row[-1]

fig, ax = plt.subplots()
ax.plot(matrix.transpose())

ax.set_xlabel("Iteration")
ax.set_ylabel("Cost")
ax.set_title("Cost per iteration")

plt.show()